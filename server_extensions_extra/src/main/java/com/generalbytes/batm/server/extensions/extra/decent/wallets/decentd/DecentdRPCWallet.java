package com.generalbytes.batm.server.extensions.extra.decent.wallets.decentd;

import com.azazar.bitcoin.jsonrpcclient.BitcoinException;
import com.generalbytes.batm.server.extensions.ICurrencies;
import com.generalbytes.batm.server.extensions.IWallet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by maros on 12.7.2017.
 */

public class DecentdRPCWallet {
    private static final Logger log = LoggerFactory.getLogger(DecentdRPCWallet.class);
    private static final String CRYPTO_CURRENCY = ICurrencies.DCT;

    public DecentdRPCWallet(String rpcURL, String accountName) {
        this.rpcURL = rpcURL;
        this.accountName = accountName;
    }

    private String rpcURL;
    private String accountName;

}