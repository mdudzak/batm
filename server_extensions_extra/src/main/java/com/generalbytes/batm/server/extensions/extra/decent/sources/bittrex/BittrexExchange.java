package com.generalbytes.batm.server.extensions.extra.decent.sources.bittrex;

import com.generalbytes.batm.server.extensions.ICurrencies;
import com.generalbytes.batm.server.extensions.IExchangeAdvanced;
import com.generalbytes.batm.server.extensions.IRateSourceAdvanced;
import com.generalbytes.batm.server.extensions.ITask;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.dto.trade.MarketOrder;
import org.knowm.xchange.dto.trade.OpenOrders;
import org.knowm.xchange.exceptions.ExchangeException;
import org.knowm.xchange.service.polling.account.PollingAccountService;
import org.knowm.xchange.service.polling.marketdata.PollingMarketDataService;
import org.knowm.xchange.service.polling.trade.PollingTradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by maros on 12.7.2017.
 */

public class BittrexExchange implements IExchangeAdvanced, IRateSourceAdvanced {
    private static final Logger log = LoggerFactory.getLogger("batm.master.BittrexExchange");

    private Exchange exchange = null;
    private Exchange exchangeForBitcoin = null;
    private String apiKey;
    private String apiSecret;

    private static HashMap<String,BigDecimal> rateAmounts = new HashMap<>();
    private static HashMap<String,Long> rateTimes = new HashMap<>();

    private static final long MAXIMUM_ALLOWED_TIME_OFFSET = 30 * 1000;

    private static volatile long lastCall = -1;
    public static final int CALL_PERIOD_MINIMUM = 2100; //cannot be called more often than once in 2 seconds


    public BittrexExchange(String apiKey, String apiSecret) {
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
    }

    private Exchange getExchangeForBitcoin() {
        if (this.exchangeForBitcoin == null) {
            ExchangeSpecification bfxSpec = new org.knowm.xchange.kraken.KrakenExchange().getDefaultExchangeSpecification();
            this.exchangeForBitcoin = ExchangeFactory.INSTANCE.createExchange(bfxSpec);
        }
        return this.exchangeForBitcoin;
    }


    private synchronized Exchange getExchange() {
        if (this.exchange == null) {
           ExchangeSpecification bfxSpec = new org.knowm.xchange.bittrex.v1.BittrexExchange().getDefaultExchangeSpecification();
            bfxSpec.setApiKey(this.apiKey);
            bfxSpec.setSecretKey(this.apiSecret);
            this.exchange = ExchangeFactory.INSTANCE.createExchange(bfxSpec);
        }
        return this.exchange;
    }

    public Set<String> getCryptoCurrencies() {
        Set<String> cryptoCurrencies = new HashSet<String>();
        cryptoCurrencies.add(ICurrencies.DCT);
        return cryptoCurrencies;
    }

    public Set<String> getFiatCurrencies() {
        Set<String> fiatCurrencies = new HashSet<>();
        fiatCurrencies.add(ICurrencies.EUR);
        return fiatCurrencies;
    }

    public String getPreferredFiatCurrency() {
        return ICurrencies.EUR;
    }

    @Override
    public synchronized BigDecimal getExchangeRateLast(String cryptoCurrency, String fiatCurrency) {
        String key = cryptoCurrency +"_" + fiatCurrency;
        synchronized (rateAmounts) {
            long now  = System.currentTimeMillis();
            BigDecimal amount = rateAmounts.get(key);
            if (amount == null) {
                BigDecimal bitcoinRate = getExchangeRateForBitcoinLastSync(fiatCurrency);
                BigDecimal result = getExchangeRateLastSync(cryptoCurrency, "BTC");

                System.out.println("BTC-DCT --- " + result);
                System.out.println("BTC-EUR --- " + bitcoinRate);

                BigDecimal sum = bitcoinRate.multiply(result);
                log.debug("Called bittrex exchange for rate: " + key + " = " + result);
                rateAmounts.put(key,sum);
                rateTimes.put(key,now+MAXIMUM_ALLOWED_TIME_OFFSET);
                return sum;
            }else {
                Long expirationTime = rateTimes.get(key);
                if (expirationTime > now) {
                    return rateAmounts.get(key);
                }else{
                    //do the job;
                    BigDecimal bitcoinRate = getExchangeRateForBitcoinLastSync(fiatCurrency);
                    BigDecimal result = getExchangeRateLastSync(cryptoCurrency, fiatCurrency);

                    BigDecimal sum = bitcoinRate.multiply(result);
                    log.debug("Called bittrex exchange for rate: " + key + " = " + result);
                    rateAmounts.put(key,sum);
                    rateTimes.put(key,now+MAXIMUM_ALLOWED_TIME_OFFSET);
                    return sum;
                }
            }
        }
    }

    private BigDecimal getExchangeRateForBitcoinLastSync(String cashCurrency) {
        PollingMarketDataService marketDataServiceBitcoin = getExchangeForBitcoin().getPollingMarketDataService();
        try {
            Ticker tickerBitcoin = marketDataServiceBitcoin.getTicker(CurrencyPair.BTC_EUR);
            return tickerBitcoin.getLast();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private BigDecimal getExchangeRateLastSync(String cryptoCurrency, String cashCurrency) {
        PollingMarketDataService marketDataService = getExchange().getPollingMarketDataService();

        try {
            Ticker ticker = marketDataService.getTicker(new CurrencyPair(cryptoCurrency,cashCurrency));
            return ticker.getLast();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BigDecimal getCryptoBalance(String cryptoCurrency) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            return BigDecimal.ZERO;
        }
        log.debug("Calling Bittrex exchange (getBalance)");

        try {
            return getExchange().getPollingAccountService().getAccountInfo().getWallet().getBalance(Currency.getInstance(cryptoCurrency)).getAvailable();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Bittrex exchange (getBalance) failed with message: " + e.getMessage());
        }
        return null;
    }

    @Override
    public BigDecimal getExchangeRateForBuy(String cryptoCurrency, String fiatCurrency) {
        BigDecimal result = calculateBuyPrice(cryptoCurrency, fiatCurrency, getMeasureCryptoAmount());
        if (result != null) {
            return result.divide(getMeasureCryptoAmount(), 2, BigDecimal.ROUND_UP);
        }
        return null;
    }

    @Override
    public BigDecimal getExchangeRateForSell(String cryptoCurrency, String fiatCurrency) {
        BigDecimal result = calculateSellPrice(cryptoCurrency, fiatCurrency, getMeasureCryptoAmount());
        if (result != null) {
            return result.divide(getMeasureCryptoAmount(), 2, BigDecimal.ROUND_DOWN);
        }
        return null;
    }

    public BigDecimal getFiatBalance(String fiatCurrency) {
        if (!ICurrencies.BTC.equalsIgnoreCase(fiatCurrency)) {
            return BigDecimal.ZERO;
        }
        log.debug("Calling Bittrex exchange (getBalance)");

        try {
            return getExchange().getPollingAccountService().getAccountInfo().getWallet().getBalance(Currency.getInstance(fiatCurrency)).getAvailable();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Bittrex exchange (getBalance) failed with message: " + e.getMessage());
        }
        return null;
    }

    public final String sendCoins(String destinationAddress, BigDecimal amount, String cryptoCurrency, String description) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            log.error("Bittrex implementation supports only " + Arrays.toString(getCryptoCurrencies().toArray()));
            return null;
        }

        log.info("Calling Bittrex exchange (withdrawal destination: " + destinationAddress + " amount: " + amount + " " + cryptoCurrency + ")");

        PollingAccountService accountService = getExchange().getPollingAccountService();
        try {
            String result = accountService.withdrawFunds(Currency.getInstance(cryptoCurrency), amount, destinationAddress);
            if (result == null) {
                log.warn("Bittrex exchange (withdrawFunds) failed with null");
                return null;
            }else if ("success".equalsIgnoreCase(result)){
                log.warn("Bittrex exchange (withdrawFunds) finished successfully");
                return "success";
            }else{
                log.warn("Bittrex exchange (withdrawFunds) failed with message: " + result);
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Bittrex exchange (withdrawFunds) failed with message: " + e.getMessage());
        }
        return null;
    }

    public String purchaseCoins(BigDecimal amount, String cryptoCurrency, String fiatCurrencyToUse, String description) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            log.error("Bittrex implementation supports only " + Arrays.toString(getCryptoCurrencies().toArray()));
            return null;
        }
        if (!ICurrencies.BTC.equalsIgnoreCase(fiatCurrencyToUse)) {
            log.error("Bittrex supports only " + ICurrencies.BTC );
            return null;
        }

        log.info("Calling Bittrex exchange (purchase " + amount + " " + cryptoCurrency + ")");
        PollingAccountService accountService = getExchange().getPollingAccountService();
        PollingTradeService tradeService = getExchange().getPollingTradeService();

        try {
            log.debug("AccountInfo as String: " + accountService.getAccountInfo().toString());

            CurrencyPair currencyPair = new CurrencyPair(cryptoCurrency, fiatCurrencyToUse);

            MarketOrder order = new MarketOrder(Order.OrderType.BID, amount, currencyPair);
            log.debug("marketOrder = " + order);

            String orderId = tradeService.placeMarketOrder(order);
            log.debug("orderId = " + orderId + " " + order);

            try {
                Thread.sleep(2000); //give exchange 2 seconds to reflect open order in order book
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // get open orders
            log.debug("Open orders:");
            boolean orderProcessed = false;
            int numberOfChecks = 0;
            while (!orderProcessed && numberOfChecks < 10) {
                boolean orderFound = false;
                OpenOrders openOrders = tradeService.getOpenOrders();
                for (LimitOrder openOrder : openOrders.getOpenOrders()) {
                    log.debug("openOrder = " + openOrder);
                    if (orderId.equals(openOrder.getId())) {
                        orderFound = true;
                        break;
                    }
                }
                if (orderFound) {
                    log.debug("Waiting for order to be processed.");
                    try {
                        Thread.sleep(3000); //don't get your ip address banned
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    orderProcessed = true;
                }
                numberOfChecks++;
            }
            if (orderProcessed) {
                return orderId;
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Bittrex exchange (purchaseCoins) failed with message: " + e.getMessage());
        }
        return null;
    }

    @Override
    public ITask createPurchaseCoinsTask(BigDecimal amount, String cryptoCurrency, String fiatCurrencyToUse, String description) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            log.error("Bittrex implementation supports only " + Arrays.toString(getCryptoCurrencies().toArray()));
            return null;
        }
        if (!ICurrencies.BTC.equalsIgnoreCase(fiatCurrencyToUse)) {
            log.error("Bittrex supports only " + ICurrencies.BTC );
            return null;
        }
        return new PurchaseCoinsTask(amount,cryptoCurrency,fiatCurrencyToUse,description);
    }

    @Override
    public String getDepositAddress(String cryptoCurrency) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            log.error("Bittrex implementation supports only " + Arrays.toString(getCryptoCurrencies().toArray()));
            return null;
        }
        PollingAccountService accountService = getExchange().getPollingAccountService();
        try {
            return accountService.requestDepositAddress(Currency.getInstance(cryptoCurrency));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String sellCoins(BigDecimal cryptoAmount, String cryptoCurrency, String fiatCurrencyToUse, String description) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            log.error("Bittrex implementation supports only " + Arrays.toString(getCryptoCurrencies().toArray()));
            return null;
        }
        if (!ICurrencies.BTC.equalsIgnoreCase(fiatCurrencyToUse)) {
            log.error("Bittrex supports only " + ICurrencies.BTC );
            return null;
        }

        log.info("Calling Bittrex exchange (sell " + cryptoAmount + " " + cryptoCurrency + ")");
        PollingAccountService accountService = getExchange().getPollingAccountService();
        PollingTradeService tradeService = getExchange().getPollingTradeService();

        try {
            log.debug("AccountInfo as String: " + accountService.getAccountInfo().toString());

            CurrencyPair currencyPair = new CurrencyPair(cryptoCurrency, fiatCurrencyToUse);

            MarketOrder order = new MarketOrder(Order.OrderType.ASK, cryptoAmount, currencyPair);
            log.debug("marketOrder = " + order);

            String orderId = tradeService.placeMarketOrder(order);
            log.debug("orderId = " + orderId + " " + order);

            try {
                Thread.sleep(2000); //give exchange 2 seconds to reflect open order in order book
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // get open orders
            log.debug("Open orders:");
            boolean orderProcessed = false;
            int numberOfChecks = 0;
            while (!orderProcessed && numberOfChecks < 10) {
                boolean orderFound = false;
                OpenOrders openOrders = tradeService.getOpenOrders();
                for (LimitOrder openOrder : openOrders.getOpenOrders()) {
                    log.debug("openOrder = " + openOrder);
                    if (orderId.equals(openOrder.getId())) {
                        orderFound = true;
                        break;
                    }
                }
                if (orderFound) {
                    log.debug("Waiting for order to be processed.");
                    try {
                        Thread.sleep(3000); //don't get your ip address banned
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    orderProcessed = true;
                }
                numberOfChecks++;
            }
            if (orderProcessed) {
                return orderId;
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Bittrex exchange (sellCoins) failed with message: " + e.getMessage());
        }
        return null;
    }

    @Override
    public ITask createSellCoinsTask(BigDecimal amount, String cryptoCurrency, String fiatCurrencyToUse, String description) {
        if (!getCryptoCurrencies().contains(cryptoCurrency)) {
            log.error("Bittrex implementation supports only " + Arrays.toString(getCryptoCurrencies().toArray()));
            return null;
        }
        if (!ICurrencies.BTC.equalsIgnoreCase(fiatCurrencyToUse)) {
            log.error("Bittrex supports only " + ICurrencies.BTC );
            return null;
        }
        return new SellCoinsTask(amount,cryptoCurrency,fiatCurrencyToUse,description);
    }

    class PurchaseCoinsTask implements ITask {
        private long MAXIMUM_TIME_TO_WAIT_FOR_ORDER_TO_FINISH = 5 * 60 * 60 * 1000; //5 hours

        private BigDecimal amount;
        private String cryptoCurrency;
        private String fiatCurrencyToUse;
        private String description;

        private String orderId;
        private String result;
        private boolean finished;

        PurchaseCoinsTask(BigDecimal amount, String cryptoCurrency, String fiatCurrencyToUse, String description) {
            this.amount = amount;
            this.cryptoCurrency = cryptoCurrency;
            this.fiatCurrencyToUse = fiatCurrencyToUse;
            this.description = description;
        }

        @Override
        public boolean onCreate() {
            log.info("Calling Bittrex exchange (purchase " + amount + " " + cryptoCurrency + ")");
            PollingAccountService accountService = getExchange().getPollingAccountService();
            PollingTradeService tradeService = getExchange().getPollingTradeService();

            try {
                log.debug("AccountInfo as String: " + accountService.getAccountInfo().toString());

                CurrencyPair currencyPair = new CurrencyPair(cryptoCurrency, fiatCurrencyToUse);

                MarketOrder order = new MarketOrder(Order.OrderType.BID, amount, currencyPair);
                log.debug("marketOrder = " + order);

                orderId = tradeService.placeMarketOrder(order);
                log.debug("orderId = " + orderId + " " + order);

                try {
                    Thread.sleep(2000); //give exchange 2 seconds to reflect open order in order book
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                log.error("Bittrex exchange (purchaseCoins) failed with message: " + e.getMessage());
            } catch (Throwable e) {
                e.printStackTrace();
            }
            return (orderId != null);
        }

        @Override
        public boolean onDoStep() {
            if (orderId == null) {
                log.debug("Giving up on waiting for trade to complete. Because it did not happen");
                finished = true;
                result = "Skipped";
                return false;
            }
            PollingTradeService tradeService = getExchange().getPollingTradeService();
            // get open orders
            boolean orderProcessed = false;
            long checkTillTime = System.currentTimeMillis() + MAXIMUM_TIME_TO_WAIT_FOR_ORDER_TO_FINISH;
            if (System.currentTimeMillis() > checkTillTime) {
                log.debug("Giving up on waiting for trade " + orderId + " to complete");
                finished = true;
                return false;
            }

            log.debug("Open orders:");
            boolean orderFound = false;
            try {
                OpenOrders openOrders = tradeService.getOpenOrders();
                for (LimitOrder openOrder : openOrders.getOpenOrders()) {
                    log.debug("openOrder = " + openOrder);
                    if (orderId.equals(openOrder.getId())) {
                        orderFound = true;
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (orderFound) {
                log.debug("Waiting for order to be processed.");
            }else{
                orderProcessed = true;
            }

            if (orderProcessed) {
                result = orderId;
                finished = true;
            }

            return result != null;
        }

        @Override
        public boolean isFinished() {
            return finished;
        }

        @Override
        public String getResult() {
            return result;
        }

        @Override
        public boolean isFailed() {
            return finished && result == null;
        }

        @Override
        public void onFinish() {
            log.debug("Purchase task finished.");
        }

        @Override
        public long getShortestTimeForNexStepInvocation() {
            return 5 * 1000; //it doesn't make sense to run step sooner than after 5 seconds
        }
    }

    class SellCoinsTask implements ITask {
        private long MAXIMUM_TIME_TO_WAIT_FOR_ORDER_TO_FINISH = 5 * 60 * 60 * 1000; //5 hours

        private BigDecimal cryptoAmount;
        private String cryptoCurrency;
        private String fiatCurrencyToUse;
        private String description;

        private String orderId;
        private String result;
        private boolean finished;

        SellCoinsTask(BigDecimal cryptoAmount, String cryptoCurrency, String fiatCurrencyToUse, String description) {
            this.cryptoAmount = cryptoAmount;
            this.cryptoCurrency = cryptoCurrency;
            this.fiatCurrencyToUse = fiatCurrencyToUse;
            this.description = description;
        }

        @Override
        public boolean onCreate() {
            log.info("Calling Bittrex exchange (sell " + cryptoAmount + " " + cryptoCurrency + ")");
            PollingAccountService accountService = getExchange().getPollingAccountService();
            PollingTradeService tradeService = getExchange().getPollingTradeService();

            try {
                log.debug("AccountInfo as String: " + accountService.getAccountInfo().toString());

                CurrencyPair currencyPair = new CurrencyPair(cryptoCurrency, fiatCurrencyToUse);

                MarketOrder order = new MarketOrder(Order.OrderType.ASK, cryptoAmount, currencyPair);
                log.debug("marketOrder = " + order);

                orderId = tradeService.placeMarketOrder(order);
                log.debug("orderId = " + orderId + " " + order);

                try {
                    Thread.sleep(2000); //give exchange 2 seconds to reflect open order in order book
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                log.error("Bittrex exchange (sellCoins) failed with message: " + e.getMessage());
            } catch (Throwable e) {
                e.printStackTrace();
            }
            return (orderId != null);
        }

        @Override
        public boolean onDoStep() {
            if (orderId == null) {
                log.debug("Giving up on waiting for trade to complete. Because it did not happen");
                finished = true;
                result = "Skipped";
                return false;
            }
            PollingTradeService tradeService = getExchange().getPollingTradeService();
            // get open orders
            boolean orderProcessed = false;
            long checkTillTime = System.currentTimeMillis() + MAXIMUM_TIME_TO_WAIT_FOR_ORDER_TO_FINISH;
            if (System.currentTimeMillis() > checkTillTime) {
                log.debug("Giving up on waiting for trade " + orderId + " to complete");
                finished = true;
                return false;
            }

            log.debug("Open orders:");
            boolean orderFound = false;
            try {
                OpenOrders openOrders = tradeService.getOpenOrders();
                for (LimitOrder openOrder : openOrders.getOpenOrders()) {
                    log.debug("openOrder = " + openOrder);
                    if (orderId.equals(openOrder.getId())) {
                        orderFound = true;
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (orderFound) {
                log.debug("Waiting for order to be processed.");
            }else{
                orderProcessed = true;
            }

            if (orderProcessed) {
                result = orderId;
                finished = true;
            }

            return result != null;
        }

        @Override
        public boolean isFinished() {
            return finished;
        }

        @Override
        public String getResult() {
            return result;
        }

        @Override
        public boolean isFailed() {
            return finished && result == null;
        }

        @Override
        public void onFinish() {
            log.debug("Sell task finished.");
        }

        @Override
        public long getShortestTimeForNexStepInvocation() {
            return 5 * 1000; //it doesn't make sense to run step sooner than after 5 seconds
        }
    }

    private BigDecimal getMeasureCryptoAmount() {
        return new BigDecimal(10);
    }

    private static void _log(Object object) {
        System.out.println(object);
    }

    
    @Override
    public BigDecimal calculateBuyPrice(String cryptoCurrency, String fiatCurrency, BigDecimal cryptoAmount) {
        waitForPossibleCall();
        PollingMarketDataService marketDataServiceBTC = getExchangeForBitcoin().getPollingMarketDataService();
        PollingMarketDataService marketDataService = getExchange().getPollingMarketDataService();
        try {
            _log("CRYPTO AMOUNT: " + cryptoAmount);
            CurrencyPair currencyPair = new CurrencyPair("DCT", "BTC");
            CurrencyPair currencyPairBTC = new CurrencyPair("BTC", "EUR");
            OrderBook orderBook = marketDataService.getOrderBook(currencyPair);
            List<LimitOrder> asks = orderBook.getAsks();
            BigDecimal targetAmount = cryptoAmount;
            BigDecimal asksTotal = BigDecimal.ZERO;
            BigDecimal tradableLimit = null;
            Collections.sort(asks, new Comparator<LimitOrder>() {
                @Override
                public int compare(LimitOrder lhs, LimitOrder rhs) {
                    return lhs.getLimitPrice().compareTo(rhs.getLimitPrice());
                }
            });

//            log.debug("Selected asks:");
            for (LimitOrder ask : asks) {
//                log.debug("ask = " + ask);
                asksTotal = asksTotal.add(ask.getTradableAmount());
                if (targetAmount.compareTo(asksTotal) <= 0) {
                    tradableLimit = ask.getLimitPrice();
                    System.out.println("BLA + " + tradableLimit);
                    break;
                }
            }

            if (tradableLimit != null) {
                log.debug("Called Bittrex exchange for BUY rate: " + cryptoCurrency + fiatCurrency + " = " + tradableLimit);
                return tradableLimit.multiply(cryptoAmount).multiply(getExchangeRateForBitcoinLastSync("EUR"));
            }
        } catch (ExchangeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    private void waitForPossibleCall() {
        long now = System.currentTimeMillis();
        if (lastCall != -1) {
            long diff = now - lastCall;
            if (diff < CALL_PERIOD_MINIMUM) {
                try {
                    long sleeping = CALL_PERIOD_MINIMUM - diff;
                    Thread.sleep(sleeping);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        lastCall = now;
        return;
    }

    @Override
    public BigDecimal calculateSellPrice(String cryptoCurrency, String fiatCurrency, BigDecimal cryptoAmount) {
        waitForPossibleCall();
        PollingMarketDataService marketDataService = getExchange().getPollingMarketDataService();
        try {
            CurrencyPair currencyPair = new CurrencyPair(cryptoCurrency, fiatCurrency);

            OrderBook orderBook = marketDataService.getOrderBook(currencyPair);
            List<LimitOrder> bids = orderBook.getBids();

            BigDecimal targetAmount = cryptoAmount;
            BigDecimal bidsTotal = BigDecimal.ZERO;
            BigDecimal tradableLimit = null;

            Collections.sort(bids, new Comparator<LimitOrder>() {
                @Override
                public int compare(LimitOrder lhs, LimitOrder rhs) {
                    return rhs.getLimitPrice().compareTo(lhs.getLimitPrice());
                }
            });

            for (LimitOrder bid : bids) {
                bidsTotal = bidsTotal.add(bid.getTradableAmount());
                if (targetAmount.compareTo(bidsTotal) <= 0) {
                    tradableLimit = bid.getLimitPrice();
                    break;
                }
            }

            if (tradableLimit != null) {
                log.debug("Called Bittrex exchange for SELL rate: " + cryptoCurrency + fiatCurrency + " = " + tradableLimit);
                return tradableLimit.multiply(cryptoAmount);
            }
        } catch (ExchangeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }
}
